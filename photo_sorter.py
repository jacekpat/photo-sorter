#! python3
# photo sorter and renamer
# search files on path and sort them by creation date
# rename files using provided prefix and incremental value
# usage: photo_sorter.py PATH [PREFIX]

import sys
import os
import datetime
import re


def getFiles(p):

    path = os.path.normpath(p)
    
    if os.path.exists(path):
        if os.path.isdir(path):
            path = path + '\\'
            files = [ os.path.join(path, f) for f in os.listdir(path + '.') if os.path.isfile(path + f) ]
            return files

        else:
            return path
    else:
        combinedPath = os.path.split(path)
 
        if os.path.exists(combinedPath[0]):
            path = combinedPath[0] + '\\'
            pattern = combinedPath[1]
            #checker = re.compile(pattern)
            files = [ os.path.join(path, f) for f in os.listdir(path + '.') if os.path.isfile(path + f) and re.match(pattern, f) ]
            return files

        else:
            sys.exit("Directory don't exist")


def renameFiles(files, prefix = ''):

    iter = 0

    for i in range(len(files)):

        # split to PATH and FILENAME
        file = os.path.split(files[i])

        # search for files with the same filename without extension
        indexOfOccurence = [idx for idx, s in enumerate(files) if file[1][:-4] in s]

        if i == indexOfOccurence[0]:

            oldFile = files[i]
            newFile =  file[0] + '\\' + prefix + str(iter).zfill(4) + file[1][-4:]
            print('   ' + os.path.basename(oldFile) + '      ' + os.path.basename(newFile)  + '      ' + str(datetime.datetime.fromtimestamp(os.path.getmtime(oldFile))) )
            #os.rename(oldFile, newFile)
        
            if len(indexOfOccurence) > 1:
                                
                for n in range(1, len(indexOfOccurence)):

                    file = os.path.split(files[indexOfOccurence[n]])
                    oldFile = files[indexOfOccurence[n]]
                    newFile = file[0] + '\\' + prefix + str(iter).zfill(4) + file[1][-4:]
                    print('   ' + os.path.basename(oldFile) + '      ' + os.path.basename(newFile)  + '      ' + str(datetime.datetime.fromtimestamp(os.path.getmtime(oldFile))) )
                    #os.rename(oldFile, newFile)

            iter += 1
                
    return


def main():
    
    l = len(sys.argv)

    if l == 2: 
        path = str(sys.argv[1])

        files = getFiles(path)
        files.sort(key = lambda t: os.path.getmtime(t))

        renameFiles(files)

    elif l == 3:
        path = str(sys.argv[1])
        prefix = sys.argv[2]

        files = getFiles(path)
        files.sort(key = lambda t: os.path.getmtime(t))
        
        renameFiles(files, prefix)

    else:
        print(" Please provide valid PATH and PREFIX ")
        print(" Usage - photo_sorter.py PATH [PREFIX] ")


if __name__ == '__main__':
    main()